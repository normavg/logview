def readFile(fname):
  filters = []
  filters.append(['1', 'START'])
  filters.append(['2', 'END'])
  
  f = open(fname)
  lines = []
  for line in f:
    lines.append(line)

  output = []
  
  index = 0
  for line in lines:
    index += 1
    for f in filters:
      if line.find(f[1]) != -1:
        #print(str(index)+':'+f[0]+':'+line)
        output.append(str(index)+':'+f[0]+':'+line)
        
  return output
