import curses
import sys

import checkpoint

traceLines = []
f = open(sys.argv[1])
for line in f:
  traceLines.append(line)
f.close()

currentPos = 0

def printSide(screen, traces, selectedSideItem):
  global currentPos
  lineno = 0
  for t in traces:
    if lineno == selectedSideItem:
      screen.addstr(lineno, 1, t[:20], curses.color_pair(1))
      currentPos = int(t.split(':')[0])
    else:
      screen.addstr(lineno, 1, t[:20])
    lineno += 1
  for n in range(5):
    traceLine = traceLines[currentPos+n-1]
    screen.addstr(n, 30, traceLine[62:150])

traces = checkpoint.readFile(sys.argv[1])

screen = curses.initscr()
curses.start_color()
curses.noecho()
curses.cbreak()
screen.keypad(1)


curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)

selectedSideItem = 0
printSide(screen, traces, selectedSideItem)
screen.refresh()

while True:
  c = screen.getch()

  if c == ord('q'):
    break
  if c == ord('a'):
    selectedSideItem += 1
    printSide(screen, traces, selectedSideItem)
  if c == ord('s'):
    selectedSideItem -= 1
    printSide(screen, traces, selectedSideItem)

  screen.refresh()

screen.keypad(0)
curses.nocbreak()
curses.echo()
curses.endwin()
